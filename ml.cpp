//--/----|----/----|----/----|----/----|----/----|----/----|----/----|----/----|
/*
Author: Kazuki Toyoda
Date: 14 Jun. 2018

This program includes header file "ml.h"

This program is for the demonstration of parameter estimation.
Dataset is generated according to a certain PDF by Monte Carlo method.
Using the dataset, two parameters are estimated by Maximum Likelihood Method.
To obtain the maximum of log-likelihood function, gradient method is used.
*/


#include "ml.h"

void ml() {
	Int_t nEvent = 2000, nRun = 500;
	Double_t x[nEvent];
	Double_t alpha = 0.5, beta = 0.5;
	Double_t xMin = -0.95, xMax = 0.95;
	Int_t nBin = 40;
	Double_t histMin = -1., histMax = 1.;
	Double_t canvX = 1150., canvY = 650.;
	Int_t mesh = 200;
	Bool_t draw;

	TGraph* estScat = new TGraph;
	estScat->SetTitle("Scatter Plot of Estimators;Alpha;Beta");
	TH1F* histAlpha = new TH1F("histAlpha", "Hist of Alpha", nBin, 0., 1.);
	histAlpha->SetTitle("Hist of Estimated Alpha;Alpha;");
	TH1F* histBeta = new TH1F("histBeta", "Hist of Beta", nBin, 0., 1.);
	histBeta->SetTitle("Hist of Estimated Beta;Beta;");
	

	for(Int_t iRun=0; iRun<nRun; iRun++) {	
		cout << "iRun = " << iRun << endl;

// Monte Carlo
		if(iRun == nRun-1) {
			draw = true;
		} else {
			draw = false;
		}
		MC(x, nEvent, alpha, beta, xMin, xMax, draw);		

// draw likelihood function
		if(iRun == nRun-1) {
			TGraph2D* graphLnL = new TGraph2D;
			for(Int_t i=0; i<100; ++i) {
				Double_t xPnt = gRandom->Uniform(0., 1.);
				Double_t yPnt = gRandom->Uniform(0., 1.);
				Double_t zPnt = LnL(x, nEvent, xPnt, yPnt, xMin, xMax);
				graphLnL->SetPoint(i,xPnt,yPnt,zPnt);
			}
			graphLnL->SetTitle("ln L;alpha;beta;ln L");
			graphLnL->SetMaximum(-1100.);
			TCanvas* canvLnL = new TCanvas("canvLnL", "Canv for lnL",
													 canvX, canvY);
			graphLnL->Draw("p0 tri2");
		}

// maximum likelihood method with gradient method
		Double_t lnL1 = -2000., lnL2 = -1800.;
		Double_t eta = .001;
		Double_t eps = .01;
		Double_t alphaEst = 0., betaEst = 0.;
		while(lnL2 - lnL1 > eps) {
			lnL1 = lnL2;
			Double_t dfdx = DefLnLbyAlpha(x, nEvent,
													alphaEst, betaEst, xMin, xMax);
			Double_t dfdy = DefLnLbyBeta(x, nEvent,
												  alphaEst, betaEst, xMin, xMax);
			alphaEst += eta*dfdx;
			betaEst += eta*dfdy;
			lnL2 = LnL(x, nEvent, alphaEst, betaEst, xMin, xMax);
		}
		estScat->SetPoint(iRun, alphaEst, betaEst);
		histAlpha->Fill(alphaEst);
		histBeta->Fill(betaEst);

// draw result of fit
		if(iRun == nRun-1) {
			TH1F* hist = new TH1F("hist", "Hist of x", nBin, histMin, histMax);
			for(Int_t iEvent=0; iEvent<nEvent; iEvent++) {
				hist->Fill(x[iEvent]);
			}

			TF1* fit = new TF1("fit", "F(x,[0],[1],[2],[3])", xMin, xMax);
			fit->SetParameters(alphaEst, betaEst, xMin, xMax);

			TLegend* legHist = new TLegend(0.1, 0.7, 0.48, 0.9);
			legHist->AddEntry(hist, "Monte Carlo Data", "fl");
			legHist->AddEntry(fit, "ML Fit Result", "l");

			TCanvas* canvHist = new TCanvas("canvHist", "Canvas for x Hist",
													  canvX, canvY);
			hist->Scale(nBin/(histMax-histMin)/nEvent);
			hist->Draw("hist");
			fit->Draw("same");
			legHist->Draw();
		
// draw contour
			cout << "calculating variance of estimators..." << endl;
			TGraph* contour = new TGraph;
			TGraph* truePnt = new TGraph;
			TGraph* estPnt = new TGraph;
			Int_t iPnt=0;
			Double_t alphaMin=alphaEst, alphaMax=alphaEst;
			Double_t betaMin=betaEst, betaMax=betaEst;
			for(Int_t ialpha=0; ialpha<mesh; ialpha++) {
				for(Int_t ibeta=0; ibeta<mesh; ibeta++) {
					Double_t meshAlpha = (Double_t)ialpha/(mesh-1);
					Double_t meshBeta = (Double_t)ibeta/(mesh-1);
					Double_t lnl = LnL(x, nEvent, meshAlpha, meshBeta, xMin, xMax);
					if(lnl>lnL2-0.55 && lnl<lnL2-0.45) {
						contour->SetPoint(iPnt, meshAlpha, meshBeta);
						if(meshAlpha<alphaMin) {alphaMin = meshAlpha;}
						if(meshAlpha>alphaMax) {alphaMax = meshAlpha;}
						if(meshBeta<betaMin) {betaMin = meshBeta;}
						if(meshBeta>betaMax) {betaMax = meshBeta;}
						iPnt++;
					}
				}
			}
			contour->SetTitle("lnL = lnLmax - 1/2;alpha;beta");
			contour->SetMarkerStyle(20);
			truePnt->SetPoint(0, alpha, beta);
			truePnt->SetMarkerStyle(21);
			estPnt->SetPoint(0, alphaEst, betaEst);
			estPnt->SetMarkerStyle(22);
			TLegend* legContour = new TLegend(0.1, 0.7, 0.48, 0.9);
			legContour->AddEntry(contour, "points where lnL = lnLmax-1/2", "p");
			legContour->AddEntry(truePnt, "True Value", "p");
			legContour->AddEntry(estPnt, "ML Fit Result", "p");
			TCanvas* canvContour = new TCanvas("canvContour", "Canv for Contour",
														  canvX, canvY);
			contour->Draw("ap");
			truePnt->Draw("psame");
			estPnt->Draw("psame");
			legContour->Draw();
			
			cout << endl << "estimated parameters of last run" << endl;
			cout << "alpha = " << alphaEst << " + " << alphaMax-alphaEst <<
				" - " << alphaEst-alphaMin << endl;
			cout << "beta = " << betaEst << " + " << betaMax-betaEst <<
				" - " << betaEst-betaMin << endl << endl;
		}
	}

// draw scattering plot of estimators
	TCanvas* canvEst = new TCanvas("canvEst", "Canv for Estimators",
											 canvX, canvY);
	canvEst->Divide(2, 2);
	canvEst->cd(1);
	estScat->SetMarkerStyle(20);
	estScat->Draw("ap");
	canvEst->cd(2);
	histBeta->Draw();
	canvEst->cd(3);
	histAlpha->Draw();
}
