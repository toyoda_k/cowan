//--/----|----/----|----/----|----/----|----/----|----/----|----/----|----/----|

Double_t F(Double_t x, Double_t alpha, Double_t beta,
			  Double_t xMin, Double_t xMax);
Double_t LnF(Double_t x, Double_t alpha, Double_t beta,
				 Double_t xMin, Double_t xMax);
Double_t DefFbyAlpha(Double_t x, Double_t alpha, Double_t beta,
							Double_t xMin, Double_t xMax);
Double_t DefFbyBeta(Double_t x, Double_t alpha, Double_t beta,
						  Double_t xMin, Double_t xMax);
void MC(Double_t* x, Int_t nData, Double_t alpha, Double_t beta, 
		  Double_t xMin, Double_t xMax, Bool_t draw);
Double_t LnL(Double_t* x, Double_t nEvent, Double_t alpha, Double_t beta,
				 Double_t xMin, Double_t xMax);
Double_t DefLnLbyAlpha(Double_t* x,Double_t nEvent, 
							  Double_t alpha, Double_t beta,
							  Double_t xMin, Double_t xMax);
Double_t DefLnLbyBeta(Double_t* x, Double_t nEvent, 
							 Double_t alpha, Double_t beta, 
							 Double_t xMin, Double_t xMax);

// implementation of each function
Double_t F(Double_t x, Double_t alpha, Double_t beta,
			  Double_t xMin, Double_t xMax) {
	Double_t nume = 1 + alpha*x + beta*x*x;
	Double_t deno = (xMax-xMin) 
		             + alpha/2*(pow(xMax,2)-pow(xMin,2)) 
		             + beta/3*(pow(xMax,3)-pow(xMin,3));
	return nume / deno;
}

Double_t LnF(Double_t x, Double_t alpha, Double_t beta
				 , Double_t xMin, Double_t xMax) {
	Double_t nume = log(1 + alpha*x + beta*x*x);
	Double_t deno = log((xMax-xMin) 
							  + alpha/2*(pow(xMax,2)-pow(xMin,2)) 
							  + beta/3*(pow(xMax,3)-pow(xMin,3)));
	return nume - deno;
}

Double_t DefFbyAlpha(Double_t x, Double_t alpha, Double_t beta,
							Double_t xMin, Double_t xMax) {
	Double_t nume = x / (1+alpha*x + beta*x*x);
	Double_t deno = (pow(xMax,2)-pow(xMin,2))/2/
		             ((xMax-xMin) + alpha/2*(pow(xMax,2)-pow(xMin,2)) 
		                          + beta/3*(pow(xMax,3)-pow(xMin,3)));
	return nume - deno;
}

Double_t DefFbyBeta(Double_t x, Double_t alpha, Double_t beta,
						  Double_t xMin, Double_t xMax) {
	Double_t nume = x*x / (1+alpha*x + beta*x*x);
	Double_t deno = (pow(xMax,3)-pow(xMin,3))/3/
                   ((xMax-xMin) + alpha/2*(pow(xMax,2)-pow(xMin,2)) 
                                + beta/3*(pow(xMax,3)-pow(xMin,3)));
	return nume - deno;
}

void MC(Double_t* x, Int_t nEvent, Double_t alpha, Double_t beta,
		  Double_t xMin, Double_t xMax, Bool_t draw) {
	Double_t fMax = 1.;
	if(draw) {
		TCanvas* canvMC = new TCanvas("canvMC", "Canv for MC", 1150, 650);
	}
	TGraph* graph1 = new TGraph;
	TGraph* graph2 = new TGraph;
	Int_t hit = 0;
	for(Int_t iEvent=0; hit<nEvent; iEvent++) {
		Double_t xCand = xMin + gRandom->Uniform(0., 1.)*(xMax-xMin);
		Double_t u = gRandom->Uniform(0., 1.) * fMax;
		Double_t f = F(xCand, alpha, beta, xMin, xMax);
		if(draw) {
			graph1->SetPoint(iEvent, xCand, u);
			graph2->SetPoint(iEvent, xCand, f);
		}
		if(u<f) {
			x[hit] = xCand;
			hit++;
		}
	}
	if(draw) {
		graph1->SetMarkerStyle(20);
		graph2->SetMarkerStyle(22);
		graph2->SetMarkerColor(2);
		graph1->Draw("ap");
		graph2->Draw("psame");
	}
}

Double_t LnL(Double_t* x, Double_t nEvent, Double_t alpha, Double_t beta,
				 Double_t xMin, Double_t xMax) {
	Double_t ret = 0;
	for(Int_t iEvent=0; iEvent<nEvent; iEvent++) {
		ret += LnF(x[iEvent], alpha, beta, xMin, xMax);
	}
	return ret;
}

Double_t DefLnLbyAlpha(Double_t* x, Double_t nEvent, 
							  Double_t alpha, Double_t beta, 
							  Double_t xMin, Double_t xMax) {
	Double_t ret = 0;
	for(Int_t iEvent=0; iEvent<nEvent; iEvent++) {
		ret += DefFbyAlpha(x[iEvent], alpha, beta, xMin, xMax);
	}
	return ret;
}

Double_t DefLnLbyBeta(Double_t* x, Double_t nEvent, 
							 Double_t alpha, Double_t beta, 
							 Double_t xMin, Double_t xMax) {
	Double_t ret = 0;
	for(Int_t iEvent=0; iEvent<nEvent; iEvent++) {
		ret += DefFbyBeta(x[iEvent], alpha, beta, xMin, xMax);
	}
	return ret;
}
